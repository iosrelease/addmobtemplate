//
//  ViewController.m
//  ObjC_AdSubscriptions
//
//  Created by Mostafizur Rahman on 3/21/18.
//  Copyright © 2018 ImageBucket. All rights reserved.
//

#import "ATBaseViewController.h"

@interface ATBaseViewController ()

@end

@implementation ATBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
