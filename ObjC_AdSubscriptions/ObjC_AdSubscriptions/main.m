//
//  main.m
//  ObjC_AdSubscriptions
//
//  Created by Mostafizur Rahman on 3/21/18.
//  Copyright © 2018 ImageBucket. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ATAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ATAppDelegate class]));
    }
}
